# wp2discord

A small plugin to send a wordpress post with a specific category to a discord server.

## Credits
wp2discord is heavily based on the "Post to Discord" plugin by Matt Weber, which can be found at
https://badecho.com/index.php/2020/10/20/automatically-posting-wordpress-posts-to-discord/"

## Usage  
The plugins has just two option: the webhook url from Discord and the Category name that you post must have to forward the post to the Discord server. The setting are on the General settings page of wordpress, at the bottom of the page.

## Future plans
Future plans include the option to add more that one category, and tie a category to a channel on the discord server (via the webhook)
